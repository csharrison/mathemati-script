var tokenize = function(input){
	return input.replace(/\(/g, ' ( ')
			.replace(/\)/g, ' ) ')
			.trim()
			.split(/\s+/);
};