allReductions = [{
	operator : "+",
	reductions: [
		"X X => (* 2 X)",
		"(* x X) (* x Y) => (* x (+ X Y))"
	]
},
{
	operator: "*",
	reductions: [
		"X X => (^ X 2)",
	]
},
{
	operator: "/",
	reductions: [
		"(* x X) y => (* (/ x y) X)",
		"y (* x X) => (* (/ y x) X)"
	]
}];

REPLACEMENT = 0;
EVALUATION  = 1;
function parseReduction(operator, str){
	var type, expr;

	type = REPLACEMENT;
	expr = str.split("=>");


	return makeReduction(operator, expr[0], expr[1]);

}
var SYM = "_S";
var LIT = "_L";
X = SYM+1;
Y = SYM+2;
x = LIT+1;
y = LIT+2;
function parseSide(left){
	left = left.replace("X", X);
	left = left.replace("Y", Y);

	left = left.replace("x", x);
	left = left.replace("y", y);

	left = left.replace("X", X);
	left = left.replace("Y", Y);

	left = left.replace("x", x);
	left = left.replace("y", y);
	return parse(left);
}
function makeReduction(operator, left, right){
	var pleft = parseSide("(" + left + ")");
	var pright = parseSide(right);
	return [pleft, pright];
}
var compiledReductions = {};
$(document).ready(function(){
	for(var i = 0; i < allReductions.length ; i++){
		var rclass = allReductions[i];
		var op = rclass.operator;

		compiledReductions[op] =  _.map(rclass.reductions, function(e){
				return parseReduction(rclass.operator, e);
		});
	}

});
function reductionMatches(args, pattern, replace){
	console.log("trying to match ", args, "with", pattern);
	if(replace === undefined) replace = {};

	if (args.length !== pattern.length) return false;

	var i;

	for(i = 0; i < args.length; i++){
		var a = args[i];
		var p = pattern[i];
		console.log("iterative: ", a, p);
		if((a.length && !p.length) || (! a.length && p.length)) return false;
		else if(a.length && p.length){
			//recursive
			console.log("RECURSIVE");
			var rec = reductionMatches(a, p, replace);

			if(! rec) return false;
		}
		else if (p.value.indexOf(SYM) !== -1 && a.type === "symbol"){
			console.log("SYMBOL");
			var s = a.value;
			var ps = p.value;
			console.log(replace);
			if (replace[ps]){
				if(replace[ps] !== s) return false;
			}else{
				replace[ps] = s;
			}
		}else if(p.value.indexOf(LIT) !== -1 && a.type === "literal"){
			console.log("LITERAL");
			var ps = p.value;
			var as = a.value;
			if(replace[ps]){
				if(replace[ps] !== as) return false;
			}else{
				replace[ps] = as;
			}
		}else if(p.value === a.value){

		}else{
			return false;
		}
	}
	return replace;
}
function replace(expr, replacements){
	if(expr.length){
		return _.map(expr, function(e){
			return replace(e, replacements);
		});
	}else if(expr.type === "symbol"){
		if(replacements[expr.value]){
			return {type: "symbol", value: replacements[expr.value]};
		}else{
			return expr;
		}
	}else if(expr.type === "literal"){
		return expr;
	}else{
		throw "bad replacement expression";
	}
}
function applyReduction(op, args){
	var reductions = compiledReductions[op.value];
	var match = null;
	for(var i = 0; i < reductions.length; i++){
		var pattern = reductions[i][0];
		var result = reductions[i][1];

		var rep = reductionMatches(args, pattern);
		if(rep){
			// apply that shit

			console.log("reduction matches!");
			console.log(rep);
			console.log(result);
			return replace(result, rep);

		}
	}
	return [op].concat(args);
}