var categorize = function(input) {
	if(!isNaN(parseInt(input, 10))){
		return {type: 'literal', value: parseInt(input, 10)};
	}else {
		return {type: 'symbol', value: input};
	}
};

var parenthesize = function(input, list){
	var token = input.shift();
	if(token === undefined){
		return list.pop();
	}else if(token === "("){
		list.push(parenthesize(input, []));
		return parenthesize(input, list);
	}else if(token === ")"){
		return list;
	}else {
		return parenthesize(input, list.concat(categorize(token)));
	}
};

var parse = function(input){
	return parenthesize(tokenize(input), []);
}