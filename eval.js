
var raw_eval = {
	"*" : function(x,y){ return x * y; },
	"/" : function(x,y){
		if(x % y === 0) return x/y;
		else return false;
	},
	"+" : function(x,y){ return x + y; },
	"-" : function(x,y){ return x - y; },
	"^" : function(x,y){ return Math.pow(x, y); }
};

var evaluate = function(inp){
	if(inp.type) return inp;

	var op = inp.shift();
	if(op.type === "literal") throw "literal in function position";
	args = _.map(inp, evaluate);

	if(raw_eval[op.value]){
		var func = raw_eval[op.value];
		var f = _.filter(args, function(e){
			return e.type === "literal";
		});

		if(f.length === args.length){
			return {
				type : "literal" ,
				value: _.reduce(_.pluck(args, "value") , function(x, r){
					return func(x, r);
				})
			};
		}
	}

	return applyReduction(op, args);
};